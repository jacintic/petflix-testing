<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
* home
*   -login
*       -home list
*       -search
*       -user settings
*       -favourites/for you
*       -watch a film
*   -signup
*       -payment
*
* admin
*   -users
*   -movies
*       -add
*       -modify
*/

//home page pre login
Route::get('/', function () {
    return view("prehome");
    //return view('welcome');
});

//home page pre login controlador
Route::post('/', function () {
    //Controlador
});

//sign up
Route::get('/signup', function () {
    return view("signup");
});

//sign up controlador
Route::post('/signup', function () {
    //Controlador
});

//login
Route::get('/login', function () {
    return view("login");
});

//login controlador
Route::post('/login', function () {
   //Controlador
});

//payment
Route::post('/payment', function () {
    return view("payment");
});

//home list 
// Route::get('/home', function () {
//     return view("home");
// });
//Route::get('/home', 'App\Http\Controllers\HomeController@getHomeMovies');
Route::post('/home', [HomeController::class, 'getHomeMovies']);
Route::get('/home', [HomeController::class, 'getHomeMovies']);


//search
Route::get('/search', function () {
    return view("search");
});

//search controlador
Route::post('/search', function () {
    //Controlador
});

//user data settigns 
Route::get('user_settings', function () {
    return view("user_settings");
});

//user data settigns controlador
Route::post('/user/settings', function () {
    //controlador
});

//favourites and for you lists
Route::get('/watchlists', function () {
    return view("watchlists");
});

//watching file interface
Route::get('/watch', function () {
    return view("watch");
});

//users list for admin
Route::get('/admin/users', function () {
    //return view("users_list");
}); 

//movies list for admin
Route::get('/admin/movies', function () {
    //return view("movies_list");
});

//add a movie admin
Route::get('/admin/movies/add', function () {
    //return view("movies_add");
});

//add a movie admin controlador
Route::post('/admin/movies/add', function () {
    //controlador
});

//modify a movie admin
Route::get('/admin/movies/modify', function () {
    //return view("movies_modify");
});

//modify a movie admin controlador
Route::post('/admin/movies/modify', function () {
    //controlador
});