<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Movie;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /*
    public function getHomeMovies()
    {
        $movie = new Movie();
        $movies = $movie::all();
        return view('home')->with('action', $movies);
    }
    */

    public function getHomeMovies(Request $request) 
    {
        $generes = ['action','comedy'];
        $collection = [];
        foreach ($generes as $genere) {
            $collection[] = $this->getGenere($genere)->get();
        }
        return view('home')->with('movieCollection', $collection);
    }

    public function getGenere($genere)
    {
        $movie = Movie::query();
        $movies = $movie->myjoin();
        $movies = $movie->genere($genere);
        return $movies;
    }
}
