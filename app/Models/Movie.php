<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    public function scopeGenereWhole($query)
    {
        return $query->orderBy('title')
            ->take(10);
	}

    public function scopeWhereParamEq($query, $param, $paramEq)
    {
        return $query->where($param, $paramEq);
	}

    public function scopeOrderby($query, $param)
    {
        return $query->orderBy($param);
	}

    public function scopeLimit($query, $n)
    {
        return $query->take($n);
	}
    public function scopeMyScope($query)
    {
        return $query->where('id','>=',1);
	}
    public function scopeMyjoin($query)
    {
        return $query->join('movies_x_genres', 'movies.id', 'id_movie')
            ->join('genres', 'id_genre', 'genres.id')
            ->select('movies.*', 'name as gname');
	}
    public function scopeGenere($query, $genre)
    {
        return $query->where('name',$genre);
	}
}
