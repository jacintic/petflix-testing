
 @extends('layouts.main')
@section('content')
        <!-- body -->
        <section class="videos container-fluid">
            <!-- articles -->
            <!-- drama -->
            <article class="drama-main mt-5">
                <div class="row mb-2">
                    <div class="col-1"></div>
                    <div class="col ms-3">
                        <h2>Favorites</h2>
                    </div>
                </div>
                <!-- drama carousel-->
                <div class="drama row">
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img1.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">Guardians
                                of the Galaxy</p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img2.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart-fill ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                X Men
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img3.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Spiderman
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img4.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Hulk
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img5.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Superman
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img6.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Mad Max
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img7.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Hard to Kill
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img8.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Terminator
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                </div>
            </article>
            <!-- comedy -->
            <article class="drama-main mt-5">
                <div class="row mb-2">
                    <div class="col-1"></div>
                    <div class="col ms-3">
                        <h2>Views</h2>
                    </div>
                </div>
                <!-- views-->
                <div class="drama row">
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img1.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">Guardians
                                of the Galaxy</p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img2.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart-fill ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                X Men
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img3.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Spiderman
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img4.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Hulk
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img5.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Superman
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img6.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Mad Max
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img7.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Hard to Kill
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center pic-container shadow">
                        <img src="img/movies/thumbnailmk2/img8.jpg" alt="Guardians of the Galaxy">
                        <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                            <i class="bi bi-heart ms-auto me-2"></i>
                            <p class="title mx-auto mt-auto mb-0">
                                Terminator
                            </p>
                            <p class="director mx-auto mb-1">Steven
                                Spilberg</p>
                        </div>
                    </div>
                </div>
            </article>
        </section>
@endsection 
