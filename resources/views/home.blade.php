@extends('layouts.main')
@section('content')
    <!-- body -->
    <section class="videos container-fluid">
        <!-- articles -->
        <!-- action -->
        @forelse ($movieCollection as $movies)
            <article class="drama-main mt-5">
                <div class="row mb-2">
                    <div class="col-1"></div>
                    <div class="col ms-3">
                        <!-- this prints the name of the genere -->
                        <h2>{{ $movies[0]->gname }}</h2>
                    </div>
                </div>
                <!-- drama carousel-->
                <div class="drama row {{$movies[0]->gname }}">
                    @forelse ($movies as $movie)
                    <a href="/watch">
                        <div class="col d-flex justify-content-center pic-container shadow">
                            <img src="{{ $movie->urlFile }}" alt="{{ $movie->title }}">
                            <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                                <i class="bi bi-heart ms-auto me-2"></i>
                                <p class="title mx-auto mt-auto mb-0">{{ $movie->title }}</p>
                            </div>
                        </div>
                    </a>
                    @empty
                        <p>No movies</p>
                    @endforelse
                </div>
            </article>
        @empty
            <p>Collection empty</p>
        @endforelse
    </section>
@endsection