@extends('layouts.main')
@section('css')
<!-- searchbar CSS -->
<link rel="stylesheet" href="css/search.css">
@endsection
    
@section('content')
<!-- body -->
<div class="searchResults container-fluid d-flex p-0 pt-5 vw-100 overflow-hidden">
    <!-- sidebar -->
    <aside class="d-flex vh-100 position-relative collapse sidebar">
        <section class="searchMenu collapse h-100">
        </section>
        <section class="searchMenu fixed collapse blk_bg overflow-hidden">
            <div class="d-flex flex-column">
                <!-- header of menu-->
                <div class="mainHeader d-flex border border-white">
                    <i class="bi bi-gear"></i>
                    <h3 class="mx-2 mb-0">Advanced Search</h3>
                </div>
                <!-- menu subsections -->
                <div class="subsection">
                    <div class="subHeader border border-white d-flex">
                        <h4>Type</h4>
                    </div>
                    <div class="sideForm form-check d-flex flex-column  border border-white">
                        <div class="d-flex">
                            <input type="checkbox" name="type" id="" class="form-check-input me-1">
                            <label class="form-check-label" for="flexCheckDefault">
                                Movies
                            </label>
                        </div>
                        <div class="d-flex">
                            <input type="checkbox" name="type" id="" class="form-check-input me-1">
                            <label class="form-check-label" for="flexCheckDefault">
                                Series
                            </label>
                        </div>
                    </div>
                </div>
                <div class="subsection">
                    <div class="subHeader border border-white d-flex">
                        <h4>Genere</h4>
                    </div>
                    <div class="sideForm form-check d-flex flex-column  border border-white">
                        <div class="d-flex">
                            <input type="checkbox" name="type" id="" class="form-check-input me-1">
                            <label class="form-check-label" for="flexCheckDefault">
                                Action
                            </label>
                        </div>
                        <div class="d-flex">
                            <input type="checkbox" name="type" id="" class="form-check-input me-1">
                            <label class="form-check-label" for="flexCheckDefault">
                                Thriller
                            </label>
                        </div>
                        <div class="d-flex">
                            <input type="checkbox" name="type" id="" class="form-check-input me-1">
                            <label class="form-check-label" for="flexCheckDefault">
                                Romance
                            </label>
                        </div>
                        <div class="d-flex">
                            <input type="checkbox" name="type" id="" class="form-check-input me-1">
                            <label class="form-check-label" for="flexCheckDefault">
                                Sci-fi
                            </label>
                        </div>
                    </div>
                </div>
                <div class="subsection">
                    <div class="subHeader border border-white d-flex">
                        <h4>Director</h4>
                    </div>
                    <div class="sideForm form-check d-flex flex-column  border border-white">
                        <input class="form-control form-control-sm" type="text" placeholder="Enter Director's Name"
                            aria-label=".form-control-sm example">
                        <div class="d-flex flex-wrap">
                            <div class="tagEl border border-white rounded d-flex">
                                <span class="director">Steven Spilberg</span>
                                <button type="button" class="btn-close btn-close-white" aria-label="Close"
                                    title="remove"></button>
                            </div>
                            <div class="tagEl border border-white rounded d-flex">
                                <span class="director">Quentin Tarantino</span>
                                <button type="button" class="btn-close btn-close-white" aria-label="Close"
                                    title="remove"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="subsection">
                    <div class="subHeader border border-white d-flex">
                        <h4>Actors</h4>
                    </div>
                    <div class="sideForm form-check d-flex flex-column  border border-white last">
                        <input class="form-control form-control-sm" type="text" placeholder="Enter Actor's Name"
                            aria-label=".form-control-sm example">
                        <div class="d-flex flex-wrap">
                            <div class="tagEl border border-white rounded d-flex">
                                <span class="director">Jim Carrey</span>
                                <button type="button" class="btn-close btn-close-white" aria-label="Close"
                                    title="remove"></button>
                            </div>
                            <div class="tagEl border border-white rounded d-flex">
                                <span class="director">Antonion Banderas</span>
                                <button type="button" class="btn-close btn-close-white" aria-label="Close"
                                    title="remove"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <nav class="toggleSearch pe-1 position-fixed border border-white">
            <i class="bi-gear"></i>
        </nav>
    </aside>
    <!-- main content (search results) -->
    <section class="p-2 mainMovies expanded">
        <div>
            <h2 class="fw-bold">Searching for Prison Break</h2>
            <span>11 results found</span>
        </div>
        <!-- results for search -->
        <div class="m-container d-flex flex-wrap">
            <div class="m-card">
                <img src="img/movies/thumbnailmk2/img1.jpg" alt="">
                <div class="m-info d-flex flex-column">
                    <i class="bi bi-heart ms-auto me-2"></i>
                    <p class="title mx-auto mt-auto">Guardians of the Galaxy</p>
                </div>
            </div>
            <div class="m-card">
                <img src="img/movies/thumbnailmk2/img2.jpg" alt="">
                <div class="m-info d-flex flex-column">
                    <i class="bi bi-heart ms-auto me-2"></i>
                    <p class="title mx-auto mt-auto">X Men</p>
                </div>
            </div>
            <div class="m-card">
                <img src="img/movies/thumbnailmk2/img3.jpg" alt="">
                <div class="m-info d-flex flex-column">
                    <i class="bi bi-heart ms-auto me-2"></i>
                    <p class="title mx-auto mt-auto">Spiderman</p>
                </div>
            </div>
            <div class="m-card">
                <img src="img/movies/thumbnailmk2/img4.jpg" alt="">
                <div class="m-info d-flex flex-column">
                    <i class="bi bi-heart ms-auto me-2"></i>
                    <p class="title mx-auto mt-auto">Hulk</p>
                </div>
            </div>
            <div class="m-card">
                <img src="img/movies/thumbnailmk2/img5.jpg" alt="">
                <div class="m-info d-flex flex-column">
                    <i class="bi bi-heart ms-auto me-2"></i>
                    <p class="title mx-auto mt-auto">SuperMan</p>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('js')
<!-- main js (toggle side menu) -->
<script src="js/search/main.js"></script>
@endsection
