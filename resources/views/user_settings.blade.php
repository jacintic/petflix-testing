@extends('layouts.main')

@section('content')
<div class="separador"></div>

<div class="container contenedor mt-3 mb-3">
    <div class="card-body">
      <form class=" d-flex flex-wrap" action="settings" method="post">
        <div class="datos">
            <h3>Edit Profile</h3>
            <div class="form-campo row">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="name" name="email" value="example@example.com">
                </div>
            </div>
            <div class="form-campo mb-3 row">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" name="name" value="santy">
                </div>
            </div>

            <div class="form-campo mb-3 row">
                <label for="lastName" class="col-sm-2 col-form-label">Last Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="lastName" name="lastName" value="pareja">
                </div>
            </div>

            <div class="form-campo mb-3 row">
                <label for="password" class="col-sm-2 col-form-label">password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" id="lastName" name="password" value="password">
                </div>
            </div>
            <div class="form-campo mb-3 row">
              <label for="birthDate" class="col-sm-2 col-form-label">password</label>
              <div class="col-sm-10">
                <input type="date" class="form-control" id="birthDate" name="birthDate" value="1991-06-26">
              </div>
          </div>
        </div>
        <div class="imagen">
          <div class="card" style="width: 18rem;">
            <img src="{{ asset('img/movies/thumbnailmk2/img4.jpg') }}" class="card-img-top" alt="...">
          </div>
          <div class="custom-input-file col-md-6 col-sm-6 col-xs-6 mt-3">
            <input type="file" id="fichero-tarifas" class="input-file" value="">
            cambiar 
            </div>
        </div>
        <input type="submit" value="save Changes" class="btn btn-primary">
    </form>
    </div>
</div>
@endsection